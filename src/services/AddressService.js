import axios from 'axios'

export default class {
    getAddresses()
    {
        return axios.get('http://testapp-api.dev/api/addresses');
    };

    deleteAddress(address)
    {
        return axios.delete('http://testapp-api.dev/api/addresses/' + address.id)
    }

    createAddress(address)
    {
        return axios.post('http://testapp-api.dev/api/addresses', address);
    }
}